# Introduction

## Purpose

The purpose of this document is to build an online system to automate:

- the build of PCR plates plans
- the mapping of each result to the correct patient identification number,
- the build of an Excel file recap.
- the build of some data visualization.

## Intended Audience:

The intended and primary audience of this document are all the people at IPD labs dealing with PCR plate plans, all the people at the DDSI. This document will be written with no technical terms (_hopefully_) for the benefit of everyone on the team. It will define the business rules of this application.

## Scope:

One of the chore at the IPD immunology lab is (_among other things_) to get the results returned by their [spectrophotometer](https://www.thermofisher.com/sn/en/home/life-science/lab-equipment/microplate-instruments/plate-readers/models/multiskan-skyhigh.html) and for each result, retrieve what is the associated patient. The patients samples are retrieved from a [96 wells PCR plate](https://www.azenta.com/products/96-well-skirted-pcr-plate). This task is actually done manually, using Excel files. The current project will try to ease and automate this process.

# Overall Description of the product:

1. Build or upload the PCR plate plan
2. Upload an Excel file containing the results returned by the spectrophotometer
3. Retrieve the resulting Excel file
4. Retrieve a scatter plot of the results

## Installation and Usage:

To run the project locally, one may want to :

- git clone the repository: `git clone ...`
- cd into the repository `cd ...`
- create a Python virtualenv `python -m venv .env`
- activate the Python virtualenv `source .env/bin/activate`
- install Dependencies: `pip install -r requirements/base.in`
- copy the file `env.sample` to `.env`
- edit the file `.env` and update all the environment variables according to your needs
- make sure to have PostgreSQL installed
- type the following commands
  - `python manage.py migrate && python manage.py createsuperuser --no-input`
  - `python manage.py runserver`
- open a web browser and visit the following url: `http://localhost:8000/api/v1/schema/swagger-ui/`


## Built With:

This product was exclusively built using [free software]()

- [Debian]()
- [Python]()
- [Django]()
- [Postgresql]()
- [Nginx]()
- [Podman]()
- [Gitlab]()

All the other dependencies can be found inside the `requirements` folder.


## Project structure:

```text
├── bandit.yaml
├── README.md
├── compose.yaml
├── .gitlal-ci.yml             # Config file for continuous Integration
├── Containerfile              
├── env.sample                 # environment variables
├── LICENSE
├── Makefile
├── manage.py                  # Django commands entry point
├── mkdocs.yaml                # Config for documentation generation
├── pytest.ini                 # Config for unit tests using Pytest
├── tox.ini                    
├── commons
│   └── utils.py               # Utility functions used the whole project
├── config                     # Global configuration folder
│   ├── asgi.py
│   ├── celery.py
│   ├── urls.py
│   ├── wsgi.py
│   └── settings                # Per environment settings
│       ├── base.py
│       ├── production.py
│       ├── test.py
│       └── wsgi.py
├── docs                        # Docs folder
│   ├── site
│   └── theme
├── logs
│   ├── access.log
│   ├── errors.log
│   └── requests.log
├── media                       # Folder for uploads
│   └── excels
└── requirements
    ├── base.in                 # Main Python requirements
    ├── production.in           # Python requirements for production server
    └── test.in                 # Python requirements for testing
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Patrick Nsukami - ptrck@nskm.xyz

<p align="right">(<a href="#readme-top">back to top</a>)</p>
