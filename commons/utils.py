import io
import math
import string
import zipfile
from itertools import product

import pandas as pd


def zip_it(plot, excel):
    """
    A function that takes a png and an excel file.
    Then, return a buffer containing a zip file containing
    the plot and the excel files.
    """
    buffer = io.BytesIO()
    with zipfile.ZipFile(
            buffer, "a",zipfile.ZIP_DEFLATED, False
    ) as zip_file:
        for file_name, data in [('plot_result.png', plot),
                                ('excel_result.xlsx', excel)]:
            zip_file.writestr(file_name, data.getvalue())
    buffer.seek(0)
    return buffer


def get_plot(_df):
    buffer = io.BytesIO()
    # plot figure
    # TODO: improve figure settings
    # https://pandas.pydata.org/pandas-docs/version/0.18/generated/pandas.DataFrame.plot.html
    ax = _df.plot(
        kind="scatter", grid=True,
        x="Patients", y="Ratio", s="Absorbance", c='DarkBlue'
    )
    ax.figure.savefig(buffer)
    buffer.seek(0)
    return buffer


def get_plot_bis(_df):
    buffer = io.BytesIO()
    # TODO: improve
    ax = _df.plot(
        kind="scatter", grid=True,
        x="Patients", y="Test results", c='DarkBlue'
    )
    ax.figure.savefig(buffer)
    buffer.seek(0)
    return buffer


def convert_to_excel(_df):
    buffer = io.BytesIO()
    with pd.ExcelWriter(buffer) as writer:
        _df.to_excel(writer, sheet_name="Sheet1")
    # ???
    buffer.seek(0)
    return buffer


def categorize_ratio_eliza(ratio):
    """
    Function used to categozize the Ratio.
    This categorization technique is used for Elisa tests
    tests results.
    """
    if ratio < 30:
        return 0
    if 30 <= ratio <= 40:
        return 2
    else:
        return 1


def categorize_ratio_wantai(ratio):
    """
    Function used to categozize the Ratio.
    This categorization technique is used for Wantai tests
    tests results.
    """
    return 0 if ratio < 1 else 1


def categorize_inhibition(inhibition):
    """
    Function used to categozize the inihibition percentage.
    This categorization technique is used for Seroneutralization tests results.
    """
    return 1 if inhibition > 30 else 0



def color_positivity(val):
    """
    Function used to return the right color (for the Excel cell).
    This function is used for IDvet tests results.
    """
    if val == 0:
        color = "red"
    elif val == 1:
        color = "green"
    else:
        color = "yellow"
    return f"background-color: {color}"


def apply_formatting(col):
    """
    Function used to apply a specific color to a specific column.
    This function is used to color all the cells inside the Positivity_IDvet column.
    This function is used for IDvet tests results.
    """
    if col.name == "Positivity_IDVet":
        return [color_positivity(v) for v in col.values]


def get_location_names():
    """
    Function used to return all the valid well names
    inside a PCR Plate. The names are returned inside a list.
    The names are returned column by column, row by row.
    """
    loc_names = [
        row+str(col) for col, row in product(
            range(1, 13), string.ascii_uppercase[:8]
        )]
    return loc_names


def divide_list_into_chunks(lst, chunk_size=8):
    lst2 = [lst[i:i+chunk_size] for i in range(0, len(lst), chunk_size)]
    return lst2

def skip_plate_values():
    """
    When parsing an Excel file containing a PCR Plate plan,
    We want to be able to skip letters used to name the rows.
    We want to be able to skip numbers used to name the cols.
    We want to be able to skip None values.
    """
    strings = list(string.ascii_uppercase[:8])
    numbers = list(range(1,13))
    numbers_str = list(map(lambda x: str(x), range(1,13)))
    null = [None]
    return strings + numbers + numbers_str + null


def read_result_file_into_list(excel_file):
    """
    Function used to read an Excel file containing
    spectrophotometer results, column by column.
    Then, return those results as a Python list of couples.
    Each couple is [location, absorbance value]
    """
    try:
        df = pd.read_excel(
            pd.ExcelFile(excel_file),
            "Résultats d'absorbance",
            nrows=9, # the Plate has 9 rows
            skiprows=[0,1,2,3,4], # the Excel file has some rows in the start that we don't need
        )
    except ValueError as err:
        if str(err) == "Worksheet named 'Résultats d'absorbancee' not found":
            raise ValueError(
                "Upload an Excel file containing a worksheet named 'Résultats d'absorbance'"
            )

    # drop the first column, the one containing the row names
    df.drop(df.columns[0], axis=1, inplace=True)
    results = []
    for col in df.columns:
        results.extend(df[col].tolist())
    return results
