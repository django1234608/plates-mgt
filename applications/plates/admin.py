from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from applications.plates.models import (
    Control, Location, Patient,
    Plate, PlateType, Profile,
    TestType)


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = [ProfileInline]


admin.site.register(Profile)
admin.site.register(Patient)
admin.site.register(Control)
admin.site.register(Location)
admin.site.register(Plate)
admin.site.register(TestType)
admin.site.register(PlateType)

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
