"""
Managers are for operations that operate on multiple instances.
"""
from django.db import models

# Create your managers here.
# https://docs.djangoproject.com/en/5.0/topics/db/managers/#custom-managers


class CustomManager(models.Manager):
    pass
