"""
Querysets are for adding operations on an existing queryset
in a chain of queryset calls.
"""
from django.db import models

# Create your querysets here.


class CustomQuerySet(models.QuerySet):
    pass
