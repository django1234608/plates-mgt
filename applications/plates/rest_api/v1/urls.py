"""
URL configuration for Plates Rest API v1
"""
from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)

from applications.plates.rest_api.v1 import views

app_name = "applications.plates"

patient_list = views.PatientViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

patient_detail = views.PatientViewSet.as_view({
    'get': 'retrieve',
    # 'put': 'update',
    'patch': 'partial_update',
    # 'delete': 'destroy'
})

plate_type_list = views.PlateTypeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

plate_type_detail = views.PlateTypeViewSet.as_view({
    'get': 'retrieve',
})

plate_list = views.PlateViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

plate_detail = views.PlateViewSet.as_view({
    'get': 'retrieve',
    #'put': 'update',
    'patch': 'partial_update',
})

control_list = views.ControlViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

control_detail = views.ControlViewSet.as_view({
    'get': 'retrieve',
})

location_list = views.LocationViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

location_detail = views.LocationViewSet.as_view({
    'get': 'retrieve',
})

ttype_list = views.TTypeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

ttype_detail = views.TTypeViewSet.as_view({
    'get': 'retrieve',
})

urlpatterns = [
    path('patients/', patient_list, name='patient-list'),
    path('patients/<int:pk>', patient_detail, name='patient-detail'),
    path('patients/import/', views.import_patient, name='import-patient'),
    path('plate-types/', plate_type_list, name='plate-type-list'),
    path('plate-types/<int:pk>', plate_type_detail, name='plate-type-detail'),
    path('plates/', plate_list, name='plate-list'),
    path('plates/<int:pk>', plate_detail, name='plate-detail'),
    path('plates/<int:pk>/fill', views.fill_plate, name='fill-plate'),
    path('plates/<int:pk>/import/', views.import_plate, name='import-plate'),
    path('plates/<int:pk>/process', views.process_plate, name='process-plate'),
    path('controls/', control_list, name='control-list'),
    path('controls/<int:pk>', control_detail, name='control-detail'),
    path('locations/', location_list, name='location-list'),
    path('locations/<int:pk>', location_detail, name='location-detail'),
    path('stats/', views.stats, name="my-stats"),
    path('test-types/', ttype_list, name='ttype-list'),
    path('test-types/<int:pk>', ttype_detail, name='ttype-detail'),
]

urlpatterns += [
    # SCHEMA
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('schema/swagger-ui/',
         SpectacularSwaggerView.as_view(url_name='applications.plates:v1:schema'),
         name='swagger-ui'
    ),
    # JWT
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
