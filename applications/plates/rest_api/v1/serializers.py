from rest_framework import serializers

from applications.plates.models import (Control, Location, Patient, Plate, PlateControl,
                     PlatePatient, PlateType)
from applications.plates.models import TestType as TType


class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = '__all__'


class ControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class PlateTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlateType
        fields = '__all__'


class TTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TType
        fields = '__all__'


class ReadPlateSerializer(serializers.ModelSerializer):
    patients = serializers.ReadOnlyField(source="located_patients")
    controls = serializers.ReadOnlyField(source="located_controls_bis")
    test = TTypeSerializer(read_only=True)
    class Meta:
        model = Plate
        fields = '__all__'


class WritePlateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plate
        fields = '__all__'


class FillPlateSerializer(serializers.Serializer):
    """
    A serializer that is used for one reason:
    validate the request body sent when filling a plate
    with patients and controls
    """
    class PatSerializer(serializers.Serializer):
        id = serializers.IntegerField()
        location_name = serializers.CharField()

    class ContSerializer(serializers.Serializer):
        id = serializers.IntegerField()
        control_name = serializers.CharField()
        location_name = serializers.CharField()

    patients = PatSerializer(many=True)
    controls = ContSerializer(many=True)
