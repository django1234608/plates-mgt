import json
import random
from collections import OrderedDict

import pandas as pd
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.http import urlencode
from rest_framework.exceptions import APIException
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.plates.rest_api.v1 import views
from applications.plates.models import Control, Patient, Plate, TestType as TType

from .fixtures import (fillpayload, my_anon_patients, my_controls,
                       my_locations, my_patients, my_plate,
                       my_plate_with_results, my_plates, my_user)

from django.apps import apps
plates_config = apps.get_app_config('plates')


@pytest.mark.django_db
def test_get_plates(my_plate):
    my_user, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'get': 'list'})
    request = factory.get(
        "/api/v1/plates/",
    )
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert response.data["count"] == 1
    atest = {'id', 'name', 'description', 'number_of_positives',
             'number_of_negatives', 'number_of_whites', 'created_at',
             'updated_at'}
    assert set(response.data["results"][0]["test"]) == atest


@pytest.mark.django_db
def test_get_plates_with_user_inside_no_group(my_plate, my_user):
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'get': 'list'})
    request = factory.get(
        "/api/v1/plates/",
    )
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    # if user is inside no group, he should see no plates
    assert response.data["count"] == 0


@pytest.mark.django_db
def test_post_plate(my_plate, my_user):
    scientist, plate_type, _ = my_plate
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'post': 'create'})
    request = factory.post(
        reverse("applications.plates:v1:plate-list"),
        {
            "description": "A dummy plate",
            "plate_type": plate_type.id,
            "test": my_plate[2].test.id,
        }
    )
    # we have 0 in the beginning
    assert Plate.objects.count() == 1
    force_authenticate(request, my_user)
    response = view(request)
    assert Plate.objects.count() == 2
    assert response.status_code == 201


@pytest.mark.django_db
def test_post_plate_with_missing_test_attribute(my_plate, my_user):
    scientist, plate_type, _ = my_plate
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'post': 'create'})
    request = factory.post(
        reverse("applications.plates:v1:plate-list"),
        {
            "description": "A dummy plate",
            "plate_type": plate_type.id,
        }
    )
    # we have 0 in the beginning
    assert Plate.objects.count() == 1
    force_authenticate(request, my_user)
    response = view(request)
    assert Plate.objects.count() == 2
    assert response.status_code == 201


@pytest.mark.django_db
def test_patch_plate1(my_plate):
    my_user, _, plate = my_plate
    factory = APIRequestFactory()
    ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    view = views.PlateViewSet.as_view({'patch': 'partial_update'})
    with open(plates_config.OUTPUT_FILE, "rb") as outf:
        excel_spectro_file = SimpleUploadedFile("file2.xlsx", outf.read())

    # when patching, you send the attribute you want to change
    request = factory.patch(
        reverse("applications.plates:v1:plate-detail", kwargs={'pk':plate.id}),
        {
            "description": "Description definitely patched",
            "excel_spectro_file": excel_spectro_file,
        }
    )
    # TODO: use another attribute instead of ._file
    assert plate.excel_spectro_file._file is None
    assert plate.excel_spectro_file.name is None
    assert plate.description == "A plate used to do Wantai test"
    force_authenticate(request, my_user)
    view(request, pk=plate.id)
    plate.refresh_from_db()
    assert plate.description == "Description definitely patched"
    assert plate.excel_spectro_file is not None
    assert plate.excel_spectro_file.name is not None
    assert plate.excel_spectro_file.file is not None


@pytest.mark.django_db
def test_patch_plate2(my_plate):
    my_user, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'patch': 'partial_update'})
    old_test_id = plate.test.id
    # randomly pick the new test_id
    s1 = set(TType.objects.values_list("id", flat=True))
    s1.remove(old_test_id)
    new_test_id = random.choice(list(s1))
    # when patching, you send the attribute you want to change
    request = factory.patch(
        reverse("applications.plates:v1:plate-detail", kwargs={'pk':plate.id}),
        {
            "test": new_test_id,
        }
    )
    assert plate.test.name == "elisa idvet"
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    plate.refresh_from_db()
    assert plate.test.id != old_test_id


@pytest.mark.django_db
def test_add_patients_and_controls_to_plate(
        my_plate, my_locations, my_patients, my_controls, fillpayload, my_user
):
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.fill_plate
    request = factory.post(
        reverse("applications.plates:v1:fill-plate", kwargs={'pk':plate.id}),
        json.dumps(fillpayload),
        content_type="application/json",
    )
    assert plate.patients.count() == 0
    assert plate.controls.count() == 0
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    assert response.status_code == 200
    msg = 'Patients and controls successfully added to plate.'
    assert response.data["message"] == msg
    assert plate.patients.count() == 1
    assert plate.controls.count() == 3


@pytest.mark.django_db
def test_add_patients_and_controls_to_plate_with_wrong_number_of_controls(
        my_plate, my_locations, my_patients, my_controls, fillpayload, my_user
):
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.fill_plate
    # fillpayload is a valid one, let' remove one control & make is invalid
    fillpayload["controls"].pop()
    request = factory.post(
        reverse("applications.plates:v1:fill-plate", kwargs={'pk':plate.id}),
        json.dumps(fillpayload),
        content_type="application/json",
    )
    assert plate.patients.count() == 0
    assert plate.controls.count() == 0
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    errmsg = "The number of Controls is not valid for the test elisa idvet"
    assert str(response.data["detail"]) == errmsg
    assert response.status_code == 500

    # assert response.status_code == 200
    # msg = 'Patients and controls successfully added to plate.'
    # assert response.data["message"] == msg
    # assert plate.patients.count() == 1
    # assert plate.controls.count() == 3


@pytest.mark.django_db
def test_plate_is_returned_with_patients_locations_and_controls_locations(
        my_plate, my_locations, my_patients, my_controls, fillpayload
):
    my_user, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.fill_plate
    request = factory.post(
        reverse("applications.plates:v1:fill-plate", kwargs={'pk':plate.id}),
        json.dumps(fillpayload),
        content_type="application/json",
    )
    assert plate.patients.count() == 0
    assert plate.controls.count() == 0
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    assert response.status_code == 200
    msg = 'Patients and controls successfully added to plate.'
    assert response.data["message"] == msg
    assert plate.patients.count() == 1
    assert plate.controls.count() == 3

    # get the plate from the API
    # check if controls and patients are also returned
    view = views.PlateViewSet.as_view({'get': 'list'})
    request = factory.get(
        "/api/v1/plates/",
    )
    force_authenticate(request, my_user)
    response = view(request)

    assert response.status_code == 200
    assert response.data["count"] == 1

    # assert we have one patient and 2 controls
    assert len(response.data["results"][0]["patients"]) == 1
    assert len(response.data["results"][0]["controls"]) == 3

    # assert location name has been added to each patient and each control
    assert "location_name" in response.data["results"][0]["patients"][0]
    assert "location_name" in response.data["results"][0]["controls"][0]


@pytest.mark.django_db
def test_import_plate(my_locations, my_controls, my_plate, my_anon_patients):
    scientist, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.import_plate

    with open(plates_config.PLATE_FILE_1, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        reverse("applications.plates:v1:import-plate", kwargs={'pk':plate.id}),
        {"plate_file": fcontent},
    )

    assert plate.patients.count() == 0
    assert plate.controls.count() == 0
    force_authenticate(request, scientist)
    response = view(request, pk=plate.id)
    plate.refresh_from_db()
    assert response.status_code == 201
    assert plate.patients.count() == 93
    assert plate.controls.count() == 3

    nw = np = nn = 0
    for ctl in response.data["controls"]:
        if ctl["control_name"] == "BLANC":
            nw += 1
        elif ctl["control_name"] == "POS":
            np += 1
        else:
            nn += 1
    # idvet: 1 white, 1 pos, 1 neg
    assert nw == 1
    assert np == 1
    assert nn == 1


@pytest.mark.django_db
def test_import_plate_with_an_unknown_patient(
    my_locations, my_controls, my_plate, my_anon_patients, my_user
):
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.import_plate
    with open(plates_config.PLATE_FILE_3, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        reverse("applications.plates:v1:import-plate", kwargs={'pk':plate.id}),
        {"plate_file": fcontent},
    )
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    errmsg = "The item: anon is neither a Control, neither a Patient"
    assert str(response.data["detail"]) == errmsg
    assert response.status_code == 500

@pytest.mark.django_db
def test_import_plate_with_empty_wells_in_between(my_locations, my_controls, my_plate, my_anon_patients, my_user):
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.import_plate
    with open(plates_config.PLATE_FILE_4, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        reverse("applications.plates:v1:import-plate", kwargs={'pk':plate.id}),
        {"plate_file": fcontent},
    )
    force_authenticate(request, my_user)
    response = view(request, pk=plate.id)
    assert len(response.data["patients"]) == 1
    assert len(response.data["controls"]) == 3
    assert response.data["patients"][0]["location_name"] == "A2"


@pytest.mark.django_db
def test_process_plate_no_results_attached(my_plate, my_user):
    """
    If we try to process a Plate,
    and this Plate does not have an Excel file attached to it
    We should raise an Error
    """
    _, _, plate = my_plate
    factory = APIRequestFactory()
    view = views.process_plate
    request = factory.get(
        reverse("applications.plates:v1:process-plate", kwargs={'pk':plate.id})
    )
    # send the request to the view
    force_authenticate(request, my_user)
    with pytest.raises(FileNotFoundError):
        view(request, pk=plate.id)


@pytest.mark.django_db
def test_process_plate_with_results(my_plate_with_results, my_user):
    """
    If we try to process a Plate,
    and this Plate does have an Excel file attached to it
    We should process it successfully
    """
    plate = my_plate_with_results
    factory = APIRequestFactory()
    view = views.process_plate
    CTs = [
        "application/json",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "image/png",
        "application/zip"
    ]
    requests = [
        factory.get(
            reverse("applications.plates:v1:process-plate", kwargs={'pk':plate.id}),
            content_type=ct
        ) for ct in CTs
    ]
    # send the requests to the view
    responses = []
    for req in requests:
        force_authenticate(req, my_user)
        resp = view(req, pk=plate.id)
        responses.append(resp)

    for resp in responses:
        assert resp.status_code == 200
        if "Content-Disposition" in resp.headers.keys():
            assert resp.headers["Content-Disposition"].startswith("attachment")

    resp0, resp1, resp2, resp3 = responses
    if requests[0].content_type == "application/json":
        expected = {
            'id', 'patients', 'controls', 'test', 'description',
            'created_at', 'updated_at', 'excel_spectro_file',
            'plate_type', 'created_by'
        }
        assert set(resp0.data.keys()) == expected
        assert "test_result" in resp0.data["patients"][0]
        assert "test_result" in resp0.data["controls"][0]


    if requests[1].content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        with pd.ExcelFile(resp1.file_to_stream) as xls:
            df = pd.read_excel(xls, "Sheet1")
            # We should have at least those 3 columns
            assert set(
                ['Patients', 'Test results', 'Positivity']
            ).issubset(df.columns)
            # TODO, find other assertions to validate
            assert len(df.index) > 1
            with open('result.xlsx', 'wb') as f:
                f.write(resp1.file_to_stream.getbuffer())
    if requests[2].content_type == "image/png":
        with open('plot.png', 'wb') as f:
            f.write(resp2.file_to_stream.getbuffer())
    else: # received a zip file
        with open('result.zip', 'wb') as f:
            f.write(resp3.file_to_stream.getbuffer())

@pytest.mark.django_db
def test_plate_found_with_filtering(my_plate):
    factory = APIRequestFactory()
    my_user, _, plate = my_plate
    search_term = "wantai"
    view = views.PlateViewSet.as_view({'get': 'list'})
    query_kwargs = {'search': search_term}
    baseurl = f'{reverse("applications.plates:v1:patient-list")}?{urlencode(query_kwargs)}'
    request = factory.get(baseurl)
    force_authenticate(request, my_user)
    response = view(request)
    assert response.data["count"] == 1


@pytest.mark.django_db
def test_plate_not_found_with_filtering(my_plate, my_user):
    factory = APIRequestFactory()
    _, _, plate = my_plate
    search_term = "foobar"
    view = views.PlateViewSet.as_view({'get': 'list'})
    query_kwargs = {'search': search_term}
    baseurl = f'{reverse("applications.plates:v1:patient-list")}?{urlencode(query_kwargs)}'
    request = factory.get(baseurl)
    force_authenticate(request, my_user)
    response = view(request)
    assert response.data["count"] == 0


@pytest.mark.django_db
def test_cant_access_without_credentials():
    err_msg = 'Authentication credentials were not provided.'
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'get': 'list'})
    query_kwargs = {'search': "foobar"}
    baseurl = f'{reverse("applications.plates:v1:patient-list")}?{urlencode(query_kwargs)}'
    request = factory.get(baseurl)
    response = view(request)
    assert response.status_code == 401
    assert str(response.data["detail"]) == err_msg


@pytest.mark.django_db
def test_get_plates_per_group(my_plates):
    plates, scientists = my_plates
    factory = APIRequestFactory()
    view = views.PlateViewSet.as_view({'get': 'list'})
    request = factory.get(
        "/api/v1/plates/",
    )
    force_authenticate(request, scientists[1])
    response = view(request)
    assert response.status_code == 200
    # there are 2 scientists in group 2
    assert response.data["count"] == 2

    force_authenticate(request, scientists[0])
    response = view(request)
    assert response.status_code == 200
    # user 1 is the only one inside the group 1
    assert response.data["count"] == 1
