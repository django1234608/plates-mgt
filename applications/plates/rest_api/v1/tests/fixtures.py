import secrets
import string
from itertools import product
from pathlib import Path

import pytest

from django.contrib.auth.models import Group, User
from django.core.files import File
from faker import Faker
from django.apps import apps


from applications.plates.models import (
    Control, Location, Patient, Plate, PlateType,
    TestType as TType)

plates_config = apps.get_app_config('plates')

@pytest.fixture
def my_patients():
    """
    Function that create 5 Patients randomly
    """
    fake = Faker()
    patient_list = [
        Patient.objects.create(
                first_name=fake.unique.user_name(),
                last_name=fake.unique.user_name(),
                anon_name=fake.unique.user_name(),
                sex=secrets.choice(['M', 'F']),
                birth_date=fake.date_of_birth(minimum_age=30)
            ) for _ in range(5)
    ]
    return patient_list


@pytest.fixture
def my_controls():
    """
    Function that create 3 Controls.
    """
    ctl_names = ("BLANC", "POS", "NEG")
    return [Control.objects.create(control_name=x) for x in ctl_names]


@pytest.fixture
def my_locations():
    """
    Function that create all valid locations names inside a 96 wells PCR Plate.
    """
    loc_names = [
        row+str(col) for col, row in product(
            range(1, 13), string.ascii_uppercase[:8]
        )]
    locations = [Location.objects.create(location_name=x) for x in loc_names]
    return locations


@pytest.fixture
def my_plate():
    """
    Function that create a Scientist, a Plate type and a Plate.
    The Plate is used to do a Wantai test
    """
    # create a scientist, a plate type and a plate
    scientist = User.objects.create_user(
        username="Nsukami",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )

    group = Group.objects.create()
    group.save()
    group.user_set.add(scientist)

    plate_type = PlateType.objects.create(
        number_rows = 8,
        number_cols = 12,
        label = "96 wells pcr plate"
    )
    test_wantai = TType.objects.create(
        name="elisa wantai",
        description="testing technique that counts certain antibodies, antigens, proteins, hormones in bodily fluids"
    )
    test_idvet = TType.objects.create(
        name="elisa idvet",
        description="testing technique that counts certain antibodies, antigens, proteins and hormones in bodily fluids"
    )
    test_sero = TType.objects.create(
        name="seroneutralization",
        description="testing technique that counts certain antibodies, antigens, proteins and hormones in bodily fluids"
    )

    plate = Plate.objects.create(
        description="A plate used to do Wantai test",
        plate_type=plate_type,
        test = test_idvet,
        created_by=scientist,
    )
    return scientist, plate_type, plate


@pytest.fixture
def fillpayload(my_patients, my_controls):
    """
    Function that create a JSON payload similar to the one
    used by the client application for filling a Plate with
    Patients and Controls.
    """
    data = {
        "patients": [
            {
                "id": my_patients[0].id,
                "location_name": "C3"
            }
        ],
        "controls": [
            {
                "id": my_controls[0].id,
                "control_name": "BLANC",
                "location_name": "A1"
            },
            {
                "id": my_controls[1].id,
                "control_name": "NEG",
                "location_name": "C1"
            },
            {
                "id": my_controls[2].id,
                "control_name": "POS",
                "location_name": "B1"
            }
        ]
    }
    return data

@pytest.fixture
def my_anon_patients():
    """
    Function used to create fake Patients
    with carefully picked anon names.
    """
    fake = Faker()
    anon_names = [
        # wantai
        "VR/23/360", "VR/23/1603", "VR/23/1612", "VR/23/1646",
        "VR/23/393", "VR/23/195", "VR/23/1611", "VR/23/1647",
        "VR/23/359", "VR/23/1614", "VR/23/197", "VR/23/1648",
        "VR/23/192", "VR/23/1604", "VR/23/1610", "VR/23/398",
        "VR/23/199", "VR/23/1602", "VR/23/177", "VR/23/1606",
        "VR/23/357", "VR/23/1615", "VR/23/1628", "VR/23/1605",
        "VR/23/193", "VR/23/1601", "VR/23/1641", "VR/23/1600",
        "VR/23/1613", "VR/23/1629",

        # idvet
        "1978083", "1978084", "1978085", "1978086", "1978087", "1978088", "1978089",
        "1978090", "1978091", "1978092", "1978093", "1978094", "1978095", "1978096",
        "1978097", "1978098", "1978099", "1978100", "1978101", "1978102", "1978103",
        "1978104", "1978105", "1978106", "1978107", "1978108", "1978109", "1978110",
        "1978111", "1978112", "1978113", "1978114", "1978115", "1978116", "1978117",
        "1978118", "1978119", "1978120", "1978121", "1978122", "1978123", "1978124",
        "1978125", "1978126", "1978127", "1978128", "1978129", "1978130", "1978131",
        "1978132", "1978133", "1978134", "1978135", "1978136", "1978137", "1978138",
        "1978139", "1978140", "1978141", "1978142", "1978143", "1978144", "1978145",
        "1978146", "1978147", "1978148", "1978149", "1978150", "1978151", "1978152",
        "1978153", "1978154", "1978155", "1978156", "1978157", "1978158", "1978159",
        "1978160", "1978161", "1978162", "1978163", "1978164", "1978165", "1978166",
        "1978167", "1978168", "1978169", "1978170", "1978171", "1978172", "1978173",
        "1978174", "1978175", "1978176", "1978177", "1978178"
    ]
    patient_list = [
        Patient.objects.create(
                first_name=fake.unique.user_name(),
                last_name=fake.unique.user_name(),
                anon_name=anon_name,
                sex=secrets.choice(['M', 'F']),
                birth_date=fake.date_of_birth(minimum_age=30)
            ) for anon_name in anon_names
    ]
    return patient_list



@pytest.fixture
def my_plate_with_results(my_plate, my_locations, my_controls, my_anon_patients):
    """
    Function that create a Scientist, a Plate type and a Plate.
    The created Plate will also have a valid Excel file attached to it.
    The created Plate will also have a valid Patients & Controls attached to it.
    The Excel file contains tests results coming from a spectrometer.
    The function will also make sure to create the Patients
    """
    _, _, plate = my_plate

    inf = plates_config.PLATE_FILE_2
    ouf = plates_config.RESULT_FILE_2
    # Add Patients & Controls to the Plate
    with open(inf, 'rb') as f:
        plate.fill_from_excel(f)

    # Attach Spectro result to the Plate
    with open(ouf, 'rb') as f:
        name = Path(ouf).stem + Path(ouf).suffix
        plate.excel_spectro_file.save(name, File(f), save=True)
        plate.save()
    return plate

@pytest.fixture
def my_user():
    """
    Function that create a User
    """
    user = User.objects.create_user(
        username="patrick",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )
    return user


@pytest.fixture
def my_plates():
    """
    Function that create 2 Groups, 3 Scientists, 3 Plates.
    """
    scientist1 = User.objects.create_user(
        username="Nsukami",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )
    scientist2 = User.objects.create_user(
        username="Patrick",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )
    scientist3 = User.objects.create_user(
        username="DiKiesse",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )
    scientists = [scientist1, scientist2, scientist3]

    group1 = Group(name="Group1")
    group2 = Group(name="Group2")
    group1.save()
    group2.save()
    group1.user_set.add(scientist1)
    group2.user_set.add(scientist2)
    group2.user_set.add(scientist3)

    plate_type = PlateType.objects.create(
        number_rows = 8,
        number_cols = 12,
        label = "96 wells pcr plate"
    )
    test_type = TType.objects.create(
        name="elisa",
        description="testing technique that counts certain antibodies, antigens, proteins & hormones in bodily fluid"
    )
    plate1 = Plate.objects.create(
        description="A plate used to do IDVet tests",
        plate_type=plate_type,
        test = test_type,
        created_by=scientist1,
    )
    plate2 = Plate.objects.create(
        description="A plate used to do IDVet tests",
        plate_type=plate_type,
        test = test_type,
        created_by=scientist2,
    )
    plate3 = Plate.objects.create(
        description="A plate used to do IDVet tests",
        plate_type=plate_type,
        test = test_type,
        created_by=scientist3,
    )
    plates = [plate1, plate2, plate3]

    return plates, scientists
