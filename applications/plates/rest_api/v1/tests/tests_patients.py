import json
import random
from collections import OrderedDict

import pandas as pd
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.http import urlencode
from rest_framework.exceptions import APIException
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.plates.rest_api.v1 import views
from applications.plates.models import Control, Patient, Plate

from .fixtures import (fillpayload, my_anon_patients, my_controls,
                       my_locations, my_patients, my_plate,
                       my_plate_with_results, my_plates, my_user)

from django.apps import apps
plates_config = apps.get_app_config('plates')


@pytest.mark.django_db
def test_post_patient(my_user):
    user = my_user
    factory = APIRequestFactory()
    view = views.PatientViewSet.as_view({'post': 'create'})
    request = factory.post(
        "/api/v1/patients/",
        json.dumps({
            'first_name': 'foo',
            'last_name': 'bar',
            'anon_name': 'foobarbaz',
            'sex': 'M',
            'birth_date': '1984-05-04',
        }),
        content_type="application/json",
    )
    # send the request to the view
    force_authenticate(request, user)
    response = view(request)
    assert response.status_code == 201
    # ??? TODO, why content type does not match ???
    # assert response.headers["Content-Type"] == "application/json"
    assert set(response.data.keys()) == {
        'id', 'first_name', 'last_name', 'anon_name', 'sex', 'birth_date', 'created_at', 'updated_at'
    }

@pytest.mark.django_db
def test_get_patient(my_user):
    factory = APIRequestFactory()
    view = views.PatientViewSet.as_view({'get': 'retrieve'})
    patient1 = Patient.objects.create(
        first_name="Nsukami",
        last_name="Patrick",
        anon_name="qux",
        sex="M",
        birth_date="2000-01-01"
    )
    request = factory.get(
        "/api/v1/patients/",
    )
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request, pk=patient1.pk)
    assert response.status_code == 200
    # ??? TODO, why content type does not match ???
    # assert response.headers["Content-Type"] == "application/json"

@pytest.mark.django_db
def test_get_all_patient(my_user):
    factory = APIRequestFactory()
    view = views.PatientViewSet.as_view({'get': 'list'})
    patients_list = [
        Patient.objects.create(
            first_name="Nsukami",
            last_name="Patrick",
            anon_name=f"anon_name{i}",
            sex="M",
            birth_date="2000-01-01"

        ) for i in range(3)
    ]
    request = factory.get(
        "/api/v1/patients/",
        content_type='application/json',
    )
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert response.data["count"] == 3
    assert len(response.data["results"]) == 3

    # ??? TODO, why content type does not match ???
    # assert response.headers["Content-Type"] == "application/json"

@pytest.mark.django_db
def test_import_patients(my_user):
    factory = APIRequestFactory()
    view = views.import_patient
    with open(plates_config.PATIENT_FILE, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        "/api/v1/patients/import/",
        {"patients_list": fcontent},
    )
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert response.data == 'Patients import successful'

@pytest.mark.django_db
def test_import_patients_with_missing_attributes(my_user):
    factory = APIRequestFactory()
    view = views.import_patient
    with open(plates_config.PATIENT_FILE_WITH_MISSING_ATTR, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        "/api/v1/patients/import/",
        {"patients_list": fcontent},
    )
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert response.data == 'Patients import successful'


@pytest.mark.django_db
def test_import_patients_with_duplicates(my_user):
    factory = APIRequestFactory()
    view = views.import_patient
    with open(plates_config.PATIENT_FILE_WITH_DUPLICATES, "rb") as pf:
        fcontent = SimpleUploadedFile("file1.xlsx", pf.read())

    request = factory.post(
        "/api/v1/patients/import/",
        {"patients_list": fcontent},
    )
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert response.data.startswith("Patients with following anon names already exist")


# TODO: the last 2 functions need to be merge in one
# TODO: https://docs.pytest.org/en/7.3.x/how-to/parametrize.html
@pytest.mark.django_db
def test_patient_found_with_filtering(my_patients, my_user):
    factory = APIRequestFactory()
    patients = my_patients
    search_terms = [
        patients[0].anon_name,
        patients[2].last_name,
        patients[1].first_name,
    ]
    view = views.PatientViewSet.as_view({'get': 'list'})
    for name in search_terms:
        query_kwargs = {'search': name}
        baseurl = f'{reverse("applications.plates:v1:patient-list")}?{urlencode(query_kwargs)}'
        request = factory.get(baseurl)
        force_authenticate(request, my_user)
        response = view(request)
        assert response.data["count"] == 1


@pytest.mark.django_db
def test_patient_not_found_with_filtering(my_patients, my_user):
    factory = APIRequestFactory()
    patients = my_patients
    search_terms = [
        "Alice",
        "Bob"
    ]
    view = views.PatientViewSet.as_view({'get': 'list'})
    for name in search_terms:
        query_kwargs = {'search': name}
        baseurl = f'{reverse("applications.plates:v1:patient-list")}?{urlencode(query_kwargs)}'
        request = factory.get(baseurl)
        force_authenticate(request, my_user)
        response = view(request)
        assert response.data["count"] == 0
