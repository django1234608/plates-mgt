import datetime

import django
import pytest

from .fixtures import *
from django.apps import apps
plates_config = apps.get_app_config('plates')

@pytest.mark.django_db
def test_cant_create_duplicate_controls():
    with pytest.raises(django.db.utils.IntegrityError):
        # UNIQUE constraint failed: plates_control.control_name
        Control.objects.create(control_name="BLANC")
        Control.objects.create(control_name="BLANC")


@pytest.mark.django_db
def test_cant_create_duplicate_locations():
    with pytest.raises(django.db.utils.IntegrityError):
        # UNIQUE constraint failed: plates_control.control_name
        Location.objects.create(location_name="A1")
        Location.objects.create(location_name="A1")

@pytest.mark.django_db
def test_patients_controls_locations_creation(my_patients, my_controls, my_locations):
    # check 1
    assert Patient.objects.count() == 5
    assert Control.objects.count() == 3
    assert Location.objects.count() == 96


@pytest.mark.django_db
def test_cant_add_two_patients_to_same_location(my_plate, my_patients, my_locations):
    _, _, plate = my_plate
    # there is no patients & no controls inside the plate in the beginning
    assert plate.patients.count() == 0

    # can't add same location twice
    with pytest.raises(django.db.utils.IntegrityError):
        # UNIQUE constraint failed: plates_platepatient.plate_id, plates_platepatient.location_id
        plate.patients.add(my_patients[0], through_defaults={"location":my_locations[0]})
        plate.patients.add(my_patients[1], through_defaults={"location":my_locations[0]})


@pytest.mark.django_db
def test_cant_create_same_location_twice(my_locations):
    # can't create the same location twice
    with pytest.raises(django.db.utils.IntegrityError):
        # django.db.utils.IntegrityError: UNIQUE constraint failed: plates_location.location_name
        Location.objects.create(location_name="A1")


@pytest.mark.django_db
def test_cant_create_same_control_twice(my_controls):
    with pytest.raises(django.db.utils.IntegrityError):
        # django.db.utils.IntegrityError: UNIQUE constraint failed: plates_location.location_name
        Control.objects.create(control_name="BLANC")


@pytest.mark.django_db
def test_add_one_patient_plate_location(my_plate, my_patients, my_locations):
    _, _, plate = my_plate
    # world in the beginning
    assert plate.patients.count() == 0
    assert Patient.objects.count() == 5

    # add a patient from the fixtures inside the plate
    plate.patients.add(my_patients[0], through_defaults={"location":my_locations[0]})
    # add another one
    plate.patients.add(my_patients[1], through_defaults={"location":my_locations[1]})
    assert plate.patients.count() == 2


@pytest.mark.django_db
def test_add_one_control_plate_location(my_plate, my_controls, my_locations):
    _, _, plate = my_plate
    # world in the beginning
    assert plate.controls.count() == 0
    assert Control.objects.count() == 3

    plate.controls.add(my_controls[0], through_defaults={"location":my_locations[0]})
    plate.controls.add(my_controls[1], through_defaults={"location":my_locations[1]})

    assert plate.controls.count() == 2


@pytest.mark.django_db
def test_cant_add_one_patient_twice_to_plate(my_plate, my_patients, my_locations):
    _, _, plate = my_plate
    # world in the beginning
    assert plate.patients.count() == 0
    assert Patient.objects.count() == 5

    with pytest.raises(django.db.utils.IntegrityError):
        # UNIQUE constraint failed: plates_platepatient.plate_id, plates_platepatient.patient_id
        # add a patient from the fixtures inside the plate
        plate.add_patient(my_patients[0], my_locations[0])
        # add the same one twice
        plate.add_patient(my_patients[0], my_locations[1])


@pytest.mark.django_db
def test_import_patients_from_excel_file():
    assert Patient.objects.count() == 0
    with open(plates_config.PATIENT_FILE_WITH_MISSING_ATTR, "rb") as pf:
        errors = Patient.import_from_excel(pf)
    assert Patient.objects.count() == 5
    assert len(errors) == 0


@pytest.mark.django_db
def test_import_patients_from_excel_file_if_patient_exists_do_nothing():
    assert Patient.objects.count() == 0
    with open(plates_config.PATIENT_FILE_WITH_DUPLICATES, "rb") as pf:
        errors = Patient.import_from_excel(pf)
    assert Patient.objects.count() == 5
    assert len(errors) == 5
