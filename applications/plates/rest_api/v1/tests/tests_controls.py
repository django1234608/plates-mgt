import json
import random
from collections import OrderedDict

import pandas as pd
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.http import urlencode
from rest_framework.exceptions import APIException
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.plates.rest_api.v1 import views
from applications.plates.models import Control, Patient, Plate

from .fixtures import (fillpayload, my_anon_patients, my_controls,
                       my_locations, my_patients, my_plate,
                       my_plate_with_results, my_plates, my_user)

from django.apps import apps
plates_config = apps.get_app_config('plates')


@pytest.mark.django_db
def test_get_controls(my_user):
    factory = APIRequestFactory()
    view = views.ControlViewSet.as_view({'get': 'list'})
    [Control.objects.create(control_name=x) for x in ("BLANC", "POS", "NEG")]
    request = factory.get(
        "/api/v1/controls/",
    )
    # get all controls
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 200
    assert len(response.data) == 3

@pytest.mark.django_db
def test_get_one_control(my_user):
    factory = APIRequestFactory()
    view = views.ControlViewSet.as_view({'get': 'retrieve'})
    [Control.objects.create(control_name=x) for x in ("BLANC", "POS", "NEG")]
    request = factory.get(
        "/api/v1/controls/",
    )
    # get one control
    force_authenticate(request, my_user)
    response = view(request, pk=Control.objects.first().id)
    assert response.status_code == 200
    # when there is one result, it is returned as a dict
    assert isinstance(response.data, dict)


@pytest.mark.django_db
def test_create_control(my_user):
    factory = APIRequestFactory()
    view = views.ControlViewSet.as_view({'post': 'create'})
    request = factory.post(
        "/api/v1/controls/",
        {'control_name': 'BLANC'},
    )
    assert Control.objects.count() == 0
    # send the request to the view
    force_authenticate(request, my_user)
    response = view(request)
    assert response.status_code == 201
    # assert we created one control
    assert Control.objects.count() == 1
    assert isinstance(response.data, dict)

    # you can't create the same control twice
    response = view(request)
    assert isinstance(response.data["control_name"], list)
    # assert there is one error
    assert len(response.data["control_name"]) == 1
    assert response.data["control_name"][0].code == "unique"
    err_msg = 'Control With This Control Name Already Exists.'
    assert response.data["control_name"][0].title() == err_msg


@pytest.mark.django_db
def test_delete_control(my_user):
    factory = APIRequestFactory()
    view = views.ControlViewSet.as_view({'delete': 'destroy'})
    [Control.objects.create(control_name=x) for x in ("BLANC", "POS", "NEG")]
    request = factory.delete(
        "/api/v1/controls/",
    )
    # we have 3 in the beginning
    assert Control.objects.count() == 3
    force_authenticate(request, my_user)
    view(request, pk=Control.objects.first().id)
    # now we have 2
    assert Control.objects.count() == 2

@pytest.mark.django_db
def test_get_stats(my_plates, my_patients):
    plates, scientists = my_plates
    factory = APIRequestFactory()
    view = views.stats
    request = factory.get(
        "/api/v1/stats/",
    )
    force_authenticate(request, scientists[0])
    response = view(request)
    assert response.status_code == 200
    content = json.loads(response.content.decode())
    content["results"]["plates_count"] == 3
    content["results"]["plates_without_result_file"] == 3

    # now attach an excel spectro file to the first plate
    ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    view = views.PlateViewSet.as_view({'patch': 'partial_update'})
    with open(plates_config.OUTPUT_FILE, "rb") as outf:
        excel_spectro_file = SimpleUploadedFile("file2.xlsx", outf.read())

    # when patching, you send the attribute you want to change
    request = factory.patch(
        reverse("applications.plates:v1:plate-detail", kwargs={'pk':plates[0].id}),
        {
            "description": "Description definitely PATCHED",
            "excel_spectro_file": excel_spectro_file,
        }
    )
    force_authenticate(request, scientists[0])
    resp = view(request, pk=plates[0].id)
    plates[0].refresh_from_db()
    # now confirm that plates w/o spectro files equals 2
    view = views.stats
    request = factory.get(
        "/api/v1/stats/",
    )
    force_authenticate(request, scientists[0])
    response = view(request)
    assert response.status_code == 200
    content = json.loads(response.content.decode())
    content["results"]["plates_count"] == 3
    content["results"]["plates_without_result_file"] == 2
