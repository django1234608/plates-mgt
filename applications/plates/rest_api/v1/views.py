import math
import operator
from functools import reduce

from django.db.models import Count, Q
from django.http import FileResponse, JsonResponse
from django.shortcuts import get_object_or_404
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import (OpenApiParameter, OpenApiResponse,
                                   extend_schema)
from rest_framework import filters, status, viewsets
from rest_framework.decorators import api_view
from rest_framework.exceptions import APIException, ParseError
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from commons.utils import (color_positivity, convert_to_excel, get_plot_bis,
                           read_result_file_into_list, zip_it)

from applications.plates.models import Control, Location, Patient, Plate, PlateType
from applications.plates.models import TestType as TType
from applications.plates.rest_api.v1.pagination import PatientPagination, PlatePagination
from applications.plates.rest_api.v1.serializers import (
    ControlSerializer, FillPlateSerializer,
    LocationSerializer, PatientSerializer,
    PlateTypeSerializer, ReadPlateSerializer,
    TTypeSerializer, WritePlateSerializer)


def not_found(request, exception, *args, **kwargs):
    """
    Generic 404 error handler.
    """
    data = {
        'error': 'Not found (404)'
    }
    return JsonResponse(data, status=status.HTTP_404_NOT_FOUND)


class PatientViewSet(viewsets.ModelViewSet):
    """
    Provides actions: `list`, `create`, `retrieve`, `update` and `destroy`
    for the Patient model.
    ---
    Filter can be applied using the search query parameter, ex `/api/v1/patients/?search=foo`,
    search is done on the following Patient fields: `first_name, last_name, anon_name`
    """
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    pagination_class = PatientPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name', 'anon_name']


class PlateTypeViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for the PlateType instance.
    """
    queryset = PlateType.objects.all()
    serializer_class = PlateTypeSerializer


class PlateViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for Plate instances.
    ---
    Filter can be applied using the search query parameter, ex `/api/v1/plates/?search=foo`,
    search is done on the following Plate field: `description`
    """
    pagination_class = PlatePagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['description',]

    def get_serializer_class(self):
         if self.request.method in ['GET']:
             return ReadPlateSerializer
         return WritePlateSerializer

    def get_queryset(self):
        user = self.request.user
        if user.groups.exists():
            filters = [Q(created_by__groups__name=g) for g in user.groups.all()]
            return Plate.objects.filter(reduce(operator.or_, filters)).all().distinct()
        else:
            return Plate.objects.none()


    def create(self, request, *args, **kwargs):
        """
        Override create action,
        set the created_by attribute to the user doing the request
        """
        ser = self.get_serializer(data=request.data)
        if ser.is_valid(raise_exception=True):
            ser.validated_data["created_by"] = self.request.user
            self.perform_create(ser)
            headers = self.get_success_headers(ser.data)
            return Response(ser.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)


class ControlViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for the Control model.
    """
    queryset = Control.objects.all()
    serializer_class = ControlSerializer


@extend_schema(
    request=FillPlateSerializer,
    responses=OpenApiResponse(
        response=OpenApiTypes.STR,
        description='A message telling if the import was successful',
    )
)
@api_view(["POST"])
def fill_plate(request, pk, *args, **kwargs):
    """
    Method to add patients and controls to a plate.
    The client sends a JSON containing
    a list of Controls and a list of Patients.
    The method should check if sent Controls exists,
    check if sent Patients exists, then, fill the PCR Plate.
    """
    if request.data:
        plate = get_object_or_404(Plate, pk=pk)
        controls = request.data["controls"]
        patients = request.data["patients"]
        # TODO: should be moved somewhere else
        # if we update the plate.test attribute, this validation is not called :(
        valid = plate.validate_number_of_controls(controls)
        if not valid:
            raise APIException(f"The number of Controls is not valid for the test {plate.test.name}")
        for obj in patients:
            if not Patient.objects.filter(pk=int(obj["id"])).exists():
                raise APIException(f"Patient with id: {obj['id']} doesn't exist.")
        for obj in controls:
            if not Control.objects.filter(pk=int(obj["id"])).exists():
                raise APIException(f"Control with id: {obj['id']} doesn't exist.")
        # if patients & controls exists,
        # clean the related items before refill
        plate.patients.clear()
        plate.controls.clear()
        for obj in patients:
            try:
                patient = Patient.objects.get(pk=int(obj["id"]))
            except Patient.DoesNotExist:
                msg = f"Patient, id: {obj['id']}, anon_name: {obj['anon_name']} does not exist."
                raise APIException(msg)
            location = get_object_or_404(
                Location, location_name=obj["location_name"]
            )
            plate.add_patient(patient, location)

        for obj in controls:
            control = get_object_or_404(Control, pk=int(obj["id"]))
            location = get_object_or_404(
                Location, location_name=obj["location_name"]
            )
            plate.add_control(control, location)

        data = {'message': 'Patients and controls successfully added to plate.'}
        return Response(data, status=200)

    data = {'message': 'Invalid JSON request.'}
    return Response(data, status=400)


# @extend_schema(responses=MyConfigSerializer)
@extend_schema()
@api_view(["GET"])
def process_plate(request, pk, *args, **kwargs):
    """
    Method to process a place once it is ready.
    Processing a Plate equals to:
    - Check if there is an Excel file attached to it
    - TODO: check is the Excel file is a valid one
    - Parse the file and extract results
    - Store each result next to each Patient
    """
    plate = get_object_or_404(Plate, pk=pk)
    if not plate.excel_spectro_file:
        raise FileNotFoundError("Excel containing Spectro results not found")
    results = read_result_file_into_list(plate.excel_spectro_file.path)
    plate_content = plate.get_as_list()
    foo = [x for x in plate_content if x[1] is not None]
    bar = [x for x in results if not math.isnan(x)]
    # TODO, this check should be done earlier: when the spectro file is uploaded
    if len(bar) != len(foo):
        msg = f"Number of items inside Spectrophotometer Excel file: {len(bar)} does not match number of items inside Plate: {len(foo)}"
        raise APIException(msg)
    plate.process(results)
    df = plate.get_dataframe()
    df_styled = df.style.map(color_positivity, subset=["Positivity"])
    excel_result = convert_to_excel(df_styled)
    plot_result = get_plot_bis(df)

    if request.content_type == "application/json":
        serialized_plate = ReadPlateSerializer(plate)
        return Response(serialized_plate.data, status=200)

    elif request.content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        return FileResponse(
            excel_result,
            filename="process_result.xlsx",
            as_attachment=True,
            content_type=request.content_type,
            status=200
        )
    elif request.content_type == "image/png":
        return FileResponse(
            plot_result,
            filename="plot_result.png",
            as_attachment=True,
            content_type=request.content_type,
            status=200
        )
    else: # return a zip file
        zip_result = zip_it(plot_result, excel_result)
        return FileResponse(
            zip_result,
            filename="process_result.zip",
            as_attachment=True,
            content_type='application/zip',
            status=200
        )



@extend_schema(
    parameters=[
        OpenApiParameter(
            name='patients_list',
            description='Excel file containing patients list with the attributes: "anon_name", "first_name", "last_name", "birth_date", "sex"',
            required=True,
            type=str),
    ],
    # description='More descriptive text',
    responses=OpenApiResponse(
        response=OpenApiTypes.STR,
        description='A message telling if the import was successful',
        #examples=[OpenApiExample('One', 1), OpenApiExample('Two', 2)],
    )
)
@api_view(["POST"])
def import_patient(request):
    """View function to import a patient list from an Excel file."""
    pfile = request.FILES.get('patients_list')
    if pfile is None:
        raise ParseError("Upload a file named patients_list")
    errors = Patient.import_from_excel(pfile)
    match errors:
        case []:
            return Response("Patients import successful")
        case _:
            msg = ", ".join([str(x) for x in errors])
            return Response(f"Patients with following anon names already exist: {msg}")


class LocationViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for the Location model.
    """
    queryset = Location.objects.all()
    serializer_class = LocationSerializer


@extend_schema(
    parameters=[
        OpenApiParameter(
            name='plate_file',
            description='Excel file containing plate content',
            required=True,
            type=str),
    ],
    responses=OpenApiResponse(
        response=OpenApiTypes.STR,
        description='A message telling if the import was successful',
    )
)
@api_view(["POST"])
def import_plate(request, pk, *args, **kwargs):
    """
    Method to import a PCR plate content from an uploaded Excel file.
    """
    pfile = request.FILES.get('plate_file')
    plate = get_object_or_404(Plate, pk=pk)
    if pfile is None:
        raise ParseError("Upload a file named plate_file")

    # before filling the plate, clear everything
    plate.patients.clear()
    plate.controls.clear()

    # TODO, I should find a way to uniform this process
    # what I'm doing here is also done with plate.validate_number_of_controls method
    # the difference is, here, we receive an Excel file, previously it was a JSON
    valid = plate.validate_number_of_controls_bis(pfile)
    if not valid:
        msg = f"The number of Controls is not valid for the test {plate.test.name}"
        raise APIException(msg)
    plate.fill_from_excel(pfile)
    data = ReadPlateSerializer(plate).data
    # TODO: should I return 200 or 201 ?
    # I didn't create the plate I just filled it
    return Response(data, status=201)


@extend_schema()
@api_view(["GET"])
def stats(request, *args, **kwargs):
    pcps = [e for e in Patient.objects.values("sex").annotate(count=Count("pk"))]
    pcpt = [e for e in Plate.objects.values("test").annotate(count=Count("pk"))]
    pcpg = [e for e in Plate.objects.values("created_by__groups__name").annotate(count=Count("pk"))]
    pwrf = Plate.objects.filter(Q(excel_spectro_file=None) | Q(excel_spectro_file="")).count()
    data = {
        'patients_count': Patient.objects.count(),
        'plates_count': Plate.objects.count(),
        'patients_count_per_gender': pcps,
        'plates_without_result_file': pwrf,
        'plates_count_per_test': pcpt,
        'plates_count_per_group': pcpg,
    }
    return JsonResponse({"results": data}, status=200)


class TTypeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for the TestType model.
    The TestType model is used to define what are
    all the available types of PCR test we can handle.
    """
    queryset = TType.objects.all()
    serializer_class = TTypeSerializer
