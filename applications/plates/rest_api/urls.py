"""
Plates API URLs.
"""

from django.urls import include, path

app_name = 'applications.plates'

urlpatterns = [
    path('v1/', include('applications.plates.rest_api.v1.urls', namespace='v1'))
]
