import datetime
from collections import Counter
from itertools import chain

import numpy as np
import pandas as pd
import polars as pl

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.utils import timezone

from rest_framework.exceptions import APIException

from commons.utils import (categorize_inhibition, categorize_ratio_eliza,
                           categorize_ratio_wantai, divide_list_into_chunks,
                           get_location_names)


# Create your models here.
class BaseModel(models.Model):
    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TestType(BaseModel):
    """
    The name of the test that will be made.
    So far, we have IDVet, Wantai and Seroneutralisation.
    Help answer the question: The Plate was made for what PCR test ?
    """
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    number_of_positives = models.IntegerField(default=1)
    number_of_negatives = models.IntegerField(default=1)
    number_of_whites = models.IntegerField(default=1)


class Patient(BaseModel):
    class Meta:
        ordering = ["-id"]

    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    anon_name = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        unique=True
    )
    sex = models.CharField(
        max_length=1,
        choices=[('M', 'Male'), ('F', 'Female')],
        default="F",
        null=True, blank=True
    )
    birth_date = models.DateField(null=True, blank=True)

    @classmethod
    def import_from_excel(cls, input_file):
        """
        Method to import Patients from an Excel file.
        """
        header_names = ["anon_name", "first_name", "last_name", "birth_date", "sex"]
        df = pd.read_excel(input_file, header=None, names=header_names)
        df["created_at"] = datetime.datetime.now()
        df["updated_at"] = datetime.datetime.now()
        df["birth_date"] = df["birth_date"].replace(np.nan, None)

        # replace NaN with None
        df = df.replace({np.nan:None})
        patients = [cls(**dict(df.iloc[i])) for i in range(len(df))]
        errors = []
        for obj in patients:
            if not cls.objects.filter(anon_name=obj.anon_name).exists():
                obj.save()
            else:
                errors.append(obj.anon_name)
        return errors


class Control(BaseModel):
    """Represent a control (white, positive, negative) inside the plate"""
    control_name = models.CharField(
        max_length=5,
        unique=True,
        choices=[('BLANC', 'BLANC'), ('POS', 'POSITIVE'), ('NEG', 'NEGATIVE')]
    )


class Profile(BaseModel):
    """
    Used to extend the default User model.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField()
    bio = models.TextField()
    lab_name = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=20)


class PlateType(BaseModel):
    number_rows = models.IntegerField(default=8)
    number_cols = models.IntegerField(default=12)
    label = models.CharField(max_length=30)


class Plate(BaseModel):
    class Meta:
        ordering = ["-id"]
    description = models.CharField(max_length=30)
    excel_spectro_file = models.FileField(upload_to='excels/', null=True, default=None)
    test = models.ForeignKey(
        "TestType",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    plate_type = models.ForeignKey(
        "PlateType",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    patients = models.ManyToManyField(
        Patient, through='PlatePatient'
    )
    controls = models.ManyToManyField(
        Control, through='PlateControl'
    )

    def get_content_at(self, lname):
        """
        A method to retrieve what is inside the well ?
        Is this a Patient ? Is this a Control ?
        """
        # loc = Location.objects.get(location_name=lname)
        try:
            obj = PlateControl.objects.get(
                plate=self, location__location_name=lname
            )
            val = obj.control.control_name
        except PlateControl.DoesNotExist:
            try:
                obj = PlatePatient.objects.get(
                    plate=self, location__location_name=lname
                )
                val = obj.patient.anon_name
            except PlatePatient.DoesNotExist:
                val = None
        return lname, val

    def get_as_list(self):
        """
        A method to generate a list containing
        all the plate content, from the first column to the last,
        from the first line to the last
        """
        data = divide_list_into_chunks(get_location_names())
        plate_as_list = []
        for column in data:
            for cell in column:
                plate_as_list.append(self.get_content_at(cell))
        return plate_as_list

    def process(self, results):
        """A method to map the plate content to the
        results of the spectrophotometer. Those results
        are stored inside the excel_file attribute."""
        plate_content = self.get_as_list()

        for content, result in zip(plate_content, results):
            location, item = content
            location_filter = Q(location__location_name=location)
            if item is None:
                continue
            elif item in ["BLANC", "POS", "NEG"]: # it is a control
                control_filter = Q(control__control_name=item)
                PlateControl.objects.filter(
                    location_filter, control_filter, plate=self,
                ).update(test_result=result)

            elif Patient.objects.filter(anon_name=item).exists(): # it is a Patient
                patient_filter = Q(patient__anon_name=item)
                PlatePatient.objects.filter(
                    location_filter, patient_filter, plate=self,
                ).update(test_result=result)

            else: # we don't know what it is, raise APIException
                raise APIException(
                    f"Something went wrong with item: {item}, at location: {location} with result: {result} when processing the Results"
                )


    def add_patient(self, patient, location):
        PlatePatient.objects.create(
            plate=self, patient=patient, location=location
        )

    def add_control(self, control, location):
        PlateControl.objects.create(
            plate=self, control=control, location=location
        )

    @property
    def located_patients(self):
        """
        Method used specifically to populate the patients field
        inside the JSON response.

        Method used to retrieve the list of all associated controls.
        This is list is then used to populate the controls field
        inside the returned JSON content.
        """
        rs = []
        for p in self.patients.all():
            pdict = model_to_dict(p)
            # birth_date could be null
            if p.birth_date:
                pdict["birth_date"] = pdict["birth_date"].isoformat()
            pp = PlatePatient.objects.get(
                patient=p, plate=self
            )
            pdict["location_name"] = pp.location.location_name
            if pp.test_result is not None:
                pdict["test_result"] = pp.test_result
            rs.append(pdict)
        return rs if rs else []

    @property
    def located_controls(self):
        """
        Method used specifically to populate the controls field
        inside the JSON response.

        Method used to retrieve the list of all associated controls.
        This is list is then used to populate the controls field
        inside the returned JSON content.
        """
        rs = []
        for c in self.controls.all():
            cdict = model_to_dict(c)
            plate_controls = PlateControl.objects.filter(
                control=c, plate=self
            )
            if len(plate_controls) == 1:
                cdict["location_name"] = plate_controls[0].location.location_name
            else:
                cdict["location_name"] = [
                    foo.location.location_name for foo in plate_controls
                ]
            # a control can appear twice in a plate,
            # but not need to add it twice in this list
            if cdict not in rs:
                rs.append(cdict)

        return rs if rs else []

    @property
    def located_controls_bis(self):
        """
        Method used specifically to populate the controls field
        inside the JSON response.

        Method used to retrieve the list of all associated controls.
        This is list is then used to populate the controls field
        inside the returned JSON content.
        """
        rs = []
        for pc in PlateControl.objects.filter(plate=self):
            cdict = model_to_dict(pc.control)
            cdict["location_name"] = pc.location.location_name
            if pc.test_result is not None:
                cdict["test_result"] = pc.test_result
            rs.append(cdict)
        return rs if rs else []


    def set_patient_result(self, pname):
        """A Method to store the test result for a patient"""
        pass

    def set_control_result(self, pname):
        """A Method to store the test result for a control"""
        pass

    def __get_dataframe_from_excel(self, input_file):
        """
        Take an Excel file containing a Plate plan,
        Build and return a clean dataframe from this file
        """
        df = pl.read_excel(
            source=input_file,
            sheet_name="Plan de plaque"
        )
        # drop the column containing the row names
        df.drop_in_place('_duplicated_0')
        return df

    def __is_valid_control(self, item):
        if item in ["BLANC","NEG","POS", None]:
            return True
        return False

    def __is_valid_patient(self, item):
        if Patient.objects.filter(anon_name=item).exists():
            return True
        return False

    def __plate_content_as_list(self, df):
        columns_content = [col.to_list() for col in df.get_columns()]
        plate_content = []
        for elt in chain.from_iterable(columns_content):
            # check if elt is a Control or a Patient, if yes, pass, if no, raise exception early
            if self.__is_valid_control(elt):
                plate_content.append(elt)
            elif self.__is_valid_patient(elt):
                plate_content.append(elt)
            else:
                raise APIException(f"The item: {elt} is neither a Control, neither a Patient")
        return plate_content

    def fill_from_excel(self, input_file):
        df = self.__get_dataframe_from_excel(input_file)
        plate_content = self.__plate_content_as_list(df)

        # TODO: check length ?!
        if len(plate_content) != len(get_location_names()):
            raise APIException(
                f"Number of items inside pcr plate plan: {len(plate_content)} does not match 96"
            )

        # TODO: Should be doable inside the previous loop
        plate_located_content = list(zip(plate_content, get_location_names()))

        for item, loc in plate_located_content:
            location = get_object_or_404(Location, location_name=loc)
            if item is None:
                continue
            elif item in ["BLANC","NEG","POS"]:
            # TODO, naming control should be a convention
                control = get_object_or_404(Control, control_name=item)
                # add control & location inside many to many rel
                self.add_control(control, location)
            # TODO, naming patient should be a convention
            elif Patient.objects.filter(anon_name=item).exists():
                # retrieve the patient from its anon name
                patient = get_object_or_404(Patient, anon_name=item)
                # add patient & location inside many to many rel
                self.add_patient(patient, location)
            else:
                # item is neither a Control, neither a Patient
                raise APIException(f"The item {item} is neither a Control, neither a Patient")

    def get_dataframe(self):
        """Returns the current Plate as a pandas DataFrame.

        Args:
          the current Plate instance

        Returns:
          A DataFrame containing 2 columns: Patients & 'Test results'
        """
        df_pat = pd.DataFrame(
            data=PlatePatient.objects.filter(plate=self).values_list("patient__anon_name", "test_result"),
            columns=["Patients", "Test results"]
        )
        df_ctl = pd.DataFrame(
            data=PlateControl.objects.filter(plate=self).values_list("control__control_name", "test_result"),
            columns=["Patients", "Test results"]
        )
        # concat patients and controls
        df = pd.concat([df_pat, df_ctl], ignore_index=True)
        # TODO: depending on the type (IDvet, Wantai, Seuronetralisation)
        # TODO: the Ratio is calculated differently
        # extract self.test.name attribute, in fct, calculate ratio
        # breakpoint()

        # filter to retrieve controls inside the dataframe
        clause_pos = df["Patients"] == "POS"
        clause_neg = df["Patients"] == "NEG"
        clause_white = df["Patients"] == "BLANC"
        negatives = [float(e) for e in df[clause_neg]["Test results"]]
        positives = [float(e) for e in df[clause_pos]["Test results"]]
        whites = [float(e) for e in df[clause_white]["Test results"]]
        if len(negatives) == 0:
            raise APIException("There should be at least one NEGATIVE Control")
        if len(positives) == 0:
            raise APIException("There should be at least one POSITIVE Control")
        if len(whites) == 0:
            raise APIException("There should be at least one WHITE Control")
        positive_control_value = sum(positives)/len(positives)
        negative_control_value = sum(negatives)/len(negatives)

        if self.test.name.lower() == "elisa idvet":
            num = df["Test results"].astype(float) - negative_control_value
            den = positive_control_value - negative_control_value
            df["Ratio"] = num / den * 100
            df["Positivity"] = df["Ratio"].apply(categorize_ratio_eliza)
        if self.test.name.lower() == "elisa wantai":
            # TODO: should be a function or a method
            df["NC"] = negative_control_value
            df["CO"] = df["NC"] + 0.16
            df["Ratio"] = df["Test results"].astype(float) / df["CO"]
            df["Positivity"] = df["Ratio"].apply(categorize_ratio_wantai)
        if self.test.name.lower() == "seroneutralization":
            df["DO Sample / DO Neg"] = df["Test results"].astype(float) - negative_control_value
            df["% inhibition"] = (1 - df["DO Sample / DO Neg"]) * 100
            df["Positivity"] = df["% inhibition"].apply(categorize_inhibition)

        return df

    def validate_number_of_controls(self, controls):
        """
        Method used specifically to validate the number of controls.
        controls is a list of dictionnaries like
        [{'id': '1', 'control_name': 'BLANC', 'location_name': 'A1'},
        {'id': '2', 'control_name': 'NEG', 'location_name': 'C1'}, ...]

        if test type is elisa idvet:
        - nb white should be 1
        - nb neg should be 1
        - nb pos should be 1
        and so on...
        """
        c = Counter([d["control_name"] for d in controls])
        if not TestType.objects.filter(name=self.test.name).exists():
            msg = f"Cannot validate control numbers for the PCR test: {self.test.name}"
            raise APIException(msg)
        if (c["BLANC"] != self.test.number_of_whites
            or c["POS"] != self.test.number_of_positives
            or c["NEG"] != self.test.number_of_negatives):
            return False
        else:
            return True

    def validate_number_of_controls_bis(self, pfile):
        """
        Method used specifically to validate the number of controls.
        here, we receive an Excel file.

        if test type is elisa idvet:
        - nb white should be 1
        - nb neg should be 1
        - nb pos should be 1
        and so on...
        """
        df = self.__get_dataframe_from_excel(pfile)
        c = Counter(self.__plate_content_as_list(df))
        if not TestType.objects.filter(name=self.test.name).exists():
            msg = f"Cannot validate control numbers for the PCR test: {self.test.name}"
            raise APIException(msg)
        if (c["BLANC"] != self.test.number_of_whites
            or c["POS"] != self.test.number_of_positives
            or c["NEG"] != self.test.number_of_negatives):
            return False
        else:
            return True


class Location(BaseModel):
    """Represent a location inside the plate"""
    location_name = models.CharField(
        max_length=3,
        unique=True,
        # TODO: restrict valid choices [('A1', 'A1'), ('A2', 'A2')...]
    )
    location_index = models.IntegerField(null=True)


class PlatePatient(BaseModel):
    plate = models.ForeignKey(Plate, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    test_result = models.CharField(max_length=30)

    class Meta:
        unique_together = [["plate", "location"], ["plate", "patient"]]


class PlateControl(BaseModel):
    plate = models.ForeignKey(Plate, on_delete=models.CASCADE)
    control = models.ForeignKey(Control, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    test_result = models.CharField(max_length=30)

    class Meta:
        unique_together = [["plate", "location"]]
