
## Application structure:

```text
├── README.md
├── admin.py                      # Configuration for the back-office
├── api.py                         
├── apps.py                       # Configuration/settings for the application
├── tasks.py                      # Tasks that needs to be done in background
├── events.py                     # Events that may be sent by the applications
├── models.py                     # Entities managed by the app (database tables)
├── managers.py
├── querysets.py
├── constants.py
├── paginations.py
├── exceptions.py                 # Custom exceptions
├── data                          # Folder containing data used by the app, mainly Excel files
├── management
│   ├── commands
│   │   ├── insert_controls.py    # Command to insert Control entities
│   │   ├── insert_locations.py   # Command to insert Location entities
│   │   ├── insert_patients.py    # Command to insert Patient entities
│   │   ├── insert_scientists.py  # Command to insert Scientist entities
│   │   ├── insert_superuser.py   # Command to insert a super user
│   │   └── insert_testtypes.py   # Command to insert TestType entities
├── migrations
│   └── 0001_initial.py           # Migration file(s) to recreate the database
├── rest_api
│   ├── urls.py
│   └── v1
│       ├── pagination.py         # Pagination configuration
│       ├── serializers.py        # used to convert complex data types into JSON, XML, ...
│       ├── urls.py               # urls for the REST API, mappings endpoint <---> view
│       ├── views.py              # views for the REST API, talks to DB, renders JSON content
│       └── tests
│           ├── data
│           ├── fixtures.py       # Content used by the tests
│           ├── tests_controls.py 
│           ├── tests_models.py
│           ├── tests_patients.py
│           └── tests_plates.py
├── signals                       # Send/receive an info when a model is saved/changed/removed
└── tests
```
