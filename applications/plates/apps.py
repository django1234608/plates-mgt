from django.apps import AppConfig
from pathlib import Path

class PlatesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "applications.plates"
    PLATES_BASE_DIR = Path(__file__).resolve().parent
    DATA_DIR = PLATES_BASE_DIR / "data"

    INPUT_FILE = DATA_DIR / "Plaque_1_IDVeT.xlsx"
    OUTPUT_FILE = DATA_DIR / "Raw_data_IDVeT.xlsx"
    PATIENT_FILE = DATA_DIR / "Patients.xlsx"
    PATIENT_FILE_WITH_MISSING_ATTR = DATA_DIR / "Patients_with_missing_attrs.xlsx"
    PATIENT_FILE_WITH_DUPLICATES = DATA_DIR / "Patients_with_duplicates.xlsx"

    PLATE_FILE_1 = DATA_DIR / "Plaque_1_IDVeT.xlsx"

    PLATE_FILE_2 = DATA_DIR / "Plaque_2_Wantai.xlsx"
    RESULT_FILE_2 = DATA_DIR / "Results_2_Wantai.xlsx"

    PLATE_FILE_3 = DATA_DIR /  "Plate_with_empty_wells_and_unknown_patient.xlsx"
    PLATE_FILE_4 = DATA_DIR /  "Plate_with_empty_wells_and_known_patient.xlsx"
