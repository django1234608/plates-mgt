import string
from itertools import product

from django.core.management.base import BaseCommand

from apps.plates.models import Location


class Command(BaseCommand):
    help = "Creates and inserts Locations inside the database"

    def handle(self, *args, **options):
        loc_names = [
            row+str(col) for col, row in product(
                range(1, 13), string.ascii_uppercase[:8]
        )]
        locations = [Location.objects.create(location_name=x) for x in loc_names]

        self.stdout.write(
            self.style.SUCCESS('Successfully inserted Locations inside database')
        )
