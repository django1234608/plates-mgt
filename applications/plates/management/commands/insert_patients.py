import secrets

from django.core.management.base import BaseCommand, CommandError
from faker import Faker

from apps.plates.models import Patient


class Command(BaseCommand):
    help = "Creates and inserts fake Patients inside the database"

    def handle(self, *args, **options):
        fake = Faker()
        for _ in range(1000):
            Patient.objects.create(
                first_name=fake.unique.user_name(),
                last_name=fake.unique.user_name(),
                sex=secrets.choice(['M', 'F']),
                birth_date=fake.date_of_birth(minimum_age=30)
            )

        self.stdout.write(
            self.style.SUCCESS('Successfully inserted Patients inside database')
        )
