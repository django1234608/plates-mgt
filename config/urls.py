"""
URL configuration for  project.
"""

from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve
from django.conf.urls import handler400, handler404, handler500

handler404 = "applications.plates.rest_api.v1.views.not_found"
handler400 = 'rest_framework.exceptions.bad_request'
handler500 = 'rest_framework.exceptions.server_error'

urlpatterns = [
    path("admin/", admin.site.urls),
    # Plates API
    path('api/', include('applications.plates.rest_api.urls')),
]


if settings.DEBUG:
    urlpatterns += [
        re_path(r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}),
        re_path(
            r"^static/(?P<path>.*)$", serve, {"document_root": settings.STATIC_ROOT}
        ),
    ]
