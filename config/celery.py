"""
Celery config for  project.

It exposes the Celery callable as a module-level variable named ``celery_application``.

For more information on this file, see
https://docs.celeryq.dev/en/v5.3.6/django/first-steps-with-django.html
"""

import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.test")

celery_application = Celery(
    "config",  #  is the actual module/folder name where celery.py belongs
)

celery_application.config_from_object("django.conf:settings", namespace="CELERY")

celery_application.conf.broker_transport_options = {"visibility_timeout": 43200}
celery_application.conf.result_backend_transport_options = {"visibility_timeout": 43200}
celery_application.conf.visibility_timeout = 43200

celery_application.autodiscover_tasks()


@celery_application.task(bind=True, ignore_result=True)
def debug_task(self):
    print(f"Request: {self.request!r}")
