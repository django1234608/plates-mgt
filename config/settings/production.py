"""
Django test settings for  project.

"""

from dotenv import load_dotenv

from .base import *

load_dotenv()

DEBUG = FALSE
